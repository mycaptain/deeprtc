#!/bin/bash

ps auxw | grep python | grep -v grep > /dev/null

if [ $? != 0 ]
then
        nohup sudo /home/ubuntu/projects/drtc/bin/python /home/ubuntu/projects/deeprtc/signalling.py > signal.log & > /dev/null

else
        sudo kill -9 $(ps ax | grep python | grep -v grep | awk '{print $1}')

        nohup sudo /home/ubuntu/projects/drtc/bin/python /home/ubuntu/projects/deeprtc/signalling.py > signal.log & > /dev/null
fi

