import logging

from flask import Flask, request, render_template, redirect, url_for, jsonify, make_response

import cfg
from utils import *

# Create Flask app
app = Flask(__name__)
app.logger.setLevel(logging.INFO)

# List to hold rooms
rooms = {}

# Flags for control flow
ROOM_IO = 0
# RC = 0 -> Normal State
# RC = 1 -> Room creation failed, room exists
# RC = 2 -> Room joined failed, room doesn't exist
# RC = 3 -> Room joined failed, room at max capacity(4)

# Only allow entering room through creation/joining
CORRECT_ROUTE = False


def set_response_headers(res):
    res.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"  # HTTP 1.1.
    res.headers["Pragma"] = "no-cache"  # HTTP 1.0.
    res.headers["Expires"] = "0"  # Proxies.
    return res


# Room entering and creation check method
@app.route('/api/ROOM_IO', methods=['GET'])
def check_status():
    global ROOM_IO

    # Get status of queried variable
    status = ROOM_IO

    # Reset said variable
    ROOM_IO = 0

    # Send status before reset
    return jsonify({"status": status})


# Route for signalling server to tell us who joined/left
@app.route('/room_control/<ctrl>', methods=['POST'])
def room_control(ctrl):
    global rooms
    req = request.json
    rl = req.get("room")

    if ctrl == "join":
        if rl in rooms.keys():
            rooms[rl] += 1
        else:
            rooms.update({rl: 1})

    else:  # ctrl == "left"
        if rl in rooms.keys():
            rooms[rl] -= 1

    rooms = dict(filter(lambda x: x[1] != 0, rooms.items()))
    app.logger.info(rooms)
    return make_response()


# Main route
@app.route("/", methods=['GET', 'POST'])
def index():
    global ROOM_IO, CORRECT_ROUTE

    CORRECT_ROUTE = False

    # If form is submitted
    if request.method == "POST":
        req = request.form

        # Get room code and action from form
        room_code = req.get("roomCode")
        action = req.get("action")

        if action == "create":
            if room_code in rooms.keys():
                # Return to home with a modal view (Room already exists)
                app.logger.info("Room already exists, redirecting")
                ROOM_IO = 1
                r = make_response(redirect(url_for('index')))
                r = set_response_headers(r)
                return r
            else:
                # Create new room and enter room
                # TODO: You'll be able to create room if original creator hasn't accepted device permissions yet.
                app.logger.info(f"{room_code} room created!")
                CORRECT_ROUTE = True
                r = make_response(redirect(url_for('render_room', room=encode(cfg.KEY, room_code))))
                r = set_response_headers(r)
                return r

        else:  # joining room
            if room_code in rooms.keys():
                # Room full
                if rooms[room_code] == 4:
                    app.logger.info("Room is full, can't join! Redirecting to home!")
                    ROOM_IO = 3
                    r = make_response(redirect(url_for('index')))
                    r = set_response_headers(r)
                    return r

                # Room exists, Join!
                app.logger.info(f"{room_code} room joined!")
                CORRECT_ROUTE = True
                r = make_response(redirect(url_for('render_room', room=encode(cfg.KEY, room_code))))
                r = set_response_headers(r)
                return r
            else:
                # Room doesn't exist, return to home with modal
                app.logger.info("Room doesn't exist, redirecting to home!")
                ROOM_IO = 2
                r = make_response(redirect(url_for('index')))
                r = set_response_headers(r)
                return r

    # Else render index page
    r = make_response(render_template('index.html'))
    r = set_response_headers(r)
    return r


# WebRTC Rooms
@app.route("/rooms/<room>")
def render_room(room):
    if CORRECT_ROUTE:  # Only allow connection when requested through index page
        app.logger.info(decode(cfg.KEY, room))
        r = make_response(render_template('webrtc.html', room_code=decode(cfg.KEY, room)))
        r = set_response_headers(r)
        return r
    else:  # Block forward button/direct url typing, etc.
        r = make_response(redirect(url_for('index')))
        r = set_response_headers(r)
        return r


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=7777)
