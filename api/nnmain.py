import io
import logging
import os

import matplotlib.pyplot as plt
import numpy as np
import onnxruntime as ort
import scipy
import soundfile as sf
from flask import Flask, request, make_response, jsonify
from flask_cors import CORS
from scipy.spatial import distance

from .nn_utils import fb_features, matcher

app = Flask(__name__)
app.logger.setLevel(logging.INFO)
CORS(app)

endpoint1 = '/dayeonie/sd'
endpoint2 = '/dayeonie/stt'


def fx_seek(self, frames, whence=os.SEEK_SET):
    self._check_if_closed()
    position = sf._snd.sf_seek(self._file, frames, whence)
    return position


def fx_get_format_from_filename(file, mode):
    format = ''
    file = getattr(file, 'name', file)
    try:
        format = os.path.splitext(file)[-1][1:]
        format = format.decode('utf-8', 'replace')
    except Exception:
        pass
    if format == 'opus':
        return 'OGG'
    if format.upper() not in sf._formats and 'r' not in mode:
        raise TypeError("No format specified and unable to get format from "
                        "file extension: {0!r}".format(file))
    return format


sf._snd =  sf._ffi.dlopen('libsndfile-1.0.29/build/libsndfile.so')
sf._subtypes['OPUS'] = 0x0064
sf.SoundFile.seek = fx_seek
sf._get_format_from_filename = fx_get_format_from_filename


# ONNX Session
diarization_model = ort.InferenceSession('models/SD0905.onnx')
embedding_model = ort.InferenceSession('models/embed.onnx')

total_spk_embs = None
speaker_vecs = []


@app.route(endpoint1, methods=['POST'])
def speaker_diarization():
    # Get request with audioblob
    audioblob = request.files['audio'].read()

    # Read audio as Numpy Array
    data, sr = sf.read(io.BytesIO(audioblob))
    app.logger.info(len(data))

    # Filter by sound level (Crude voice activity detection)
    app.logger.info(np.mean(abs(data)))
    if np.mean(abs(data)) < 0.01:
        response = {
            "status_code": 200,
            "message": "Silence!",
            "speakers": [x.tolist() for x in speaker_vecs]
        }

        return jsonify(response)

    # Pre-process the audioblob and extract filter banks
    in_feat = fb_features((sr, data[int(0.65*sr):][::3]), 0.025, 0.01, 512, 345).astype(np.float32)
    app.logger.info(in_feat.shape)
    plt.imshow(in_feat.T)
    plt.show()
    in_feat = np.expand_dims(in_feat, 1)

    # Run the extracted features through the trained embedding network
    ort_inputs = {embedding_model.get_inputs()[0].name: in_feat}
    embed_vector = embedding_model.run(None, ort_inputs)[0][0]

    distances = []

    # For a new session, add the first 'speaker' to the list of speakers
    if len(speaker_vecs) == 0:
        speaker_vecs.append(embed_vector)
    else:
        ncount = 0
        for sidx, svec in enumerate(speaker_vecs):

            dist = distance.cosine(embed_vector, svec)
            if dist < 0.2:
                speaker_vecs[sidx] = (svec + embed_vector)/2
            else:
                ncount += 1
            distances.append(dist)
        
        if ncount > 0:
            app.logger.info("NEW SPEAKER!")
            speaker_vecs.append(embed_vector)

    response = {
        "status_code": 200,
        "message": "Audio Received and Processed!",
        "distances": distances,
        "speakers": [x.tolist() for x in speaker_vecs],
        "speaker": np.argmin(distances)
    }

    return jsonify(response)


@app.route("/endpoint1", methods=['POST'])
def speaker_diarization2():
    global total_spk_embs

    # Model Parameters
    MAX_SPEAKERS = 12
    ATTRACTOR_THRESH = 0.6
    DIAR_THRESH = 0.3
    SIM_THRESH = 0.1
    # Get file from multipart form request with key 'audio'
    audioblob = request.files['audio'].read()

    # Read audio as Numpy Array
    data, sr = sf.read(io.BytesIO(audioblob), dtype='int16')

    # Pre-process the input audio and extract mel scale filter banks
    in_feat = fb_features((sr, data[:int(0.5*sr)]), 0.025, 0.01, 512, 23, 7).astype(np.float32)[:10]
    in_feat = np.expand_dims(in_feat, 1)
    
    # Set the filters as input to the Diarization model
    ort_inputs = {diarization_model.get_inputs()[0].name: in_feat, diarization_model.get_inputs()[1].name: np.array(MAX_SPEAKERS)}
    
    # Get Results!
    diarization_prob, attractor_prob, embeddings = diarization_model.run(None, ort_inputs)
    diarization_prob = diarization_prob[0]
    attractor_prob = attractor_prob[0]
    embeddings = embeddings[:, 0, :]

    # Get num_speakers from attractor output
    num_speaker = np.where(attractor_prob < ATTRACTOR_THRESH)[0][0]
    if num_speaker > 0 :

        # Parse output, extract up to num_speaker diarization result
        diar = diarization_prob[:, :num_speaker]
        diar_thr = np.where(diar < DIAR_THRESH, 0, 1)

        # TODO: Whether to use diar or diar_thr
        # TODO: embeddings normalize or not
        spk_embs = np.matmul(diar_thr.T, embeddings)

        if total_spk_embs is None:
            total_spk_embs = spk_embs

        else:
            # total_spk_embs : (S, 128)
            # spk_embs : (Q, 128)
            # spk_em'bs랑 total_spk_embs랑 "각 스피커당" 임베딩 거리 비교 -> threshold 이하면 같은 스피커로 인정 -> 순서 변경

            # Initialize dictionary holding existing speakers as keys
            found_pairs = {x : [] for x in range(len(total_spk_embs))}
            # print("SPK", spk_embs) 
            # print("TOTAL", total_spk_embs)

            # Calculate cosine similarity between new result and exisiting speakers
            cos_sim = scipy.spatial.distance.cdist(spk_embs, total_spk_embs, 'cosine')
            # print(cos_sim)
            # If the distance is under the similarity threshold, add to possible match found, if none, add to new speaker
            new_speak = []
            for n_spk in range(len(spk_embs)):
                no_match = 0
                for o_spk in range(len(total_spk_embs)):
                    if cos_sim[n_spk][o_spk] < SIM_THRESH:
                        found_pairs[o_spk].append((n_spk, cos_sim[n_spk][o_spk]))
                    else:
                        no_match +=1
                if no_match == len(total_spk_embs):
                    new_speak.append(n_spk)

            # Filter out original speaker who didn't speak this time
            no_speak = []
            for k, v in found_pairs.items():
                if len(v) == 0:
                    no_speak.append(k)
            
            for k in no_speak:
                del found_pairs[k]
            
            if len(found_pairs) > 0:
                # Finalize the matching based on the cosine distance between the two
                final_assignment = {}
                matcher(found_pairs, final_assignment)

                # new_spaker = list of index of model output that is new
                # no_spekaer = list of index from existing speakers that didn't speak
                # final_assignment = dictionary of {exist:new} that matched
                
                # Update the existing speaker embeddings with the new embeddings
                for k, v in final_assignment.items():
                    total_spk_embs[k] = (total_spk_embs[k] + spk_embs[v]) / 2

                # Add new speaker to the exisitng speaker embeddings and final assignment
                for new_s in new_speak:
                    total_spk_embs = np.concatenate([total_spk_embs, spk_embs[new_s]])
                    final_assignment.update({len(total_spk_embs): new_s})

                # Add dummys for no speakers to final assignment    
                for i, no in enumerate(no_speak):
                    final_assignment.update({no: len(total_spk_embs) - (i+1)})
                    
                # Add the dummies to the speaker diarization results
                diar = np.concatenate([diar, np.zeros((diar.shape[0], len(no_speak)))])

                # Reverse the order {new: exist}
                fa_rev = {v: k for k, v in final_assignment.items()}
                
                # Sort by existing
                fa_rev = [x[0] for x in sorted(fa_rev.items(), key=lambda x: x[1])]

                # Permute the diarization results according to the assignment
                diar = diar[:, fa_rev]
                # print(diar)

    return jsonify(status_code=200, message="Audio Received and Saved!")


@app.route(endpoint2, methods=['POST'])
def STT():
    
    return make_response("STT")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=1999)