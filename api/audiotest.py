import soundfile as sf
import matplotlib.pyplot as plt
import io
import os

endpoint1 = '/dayeonie/sd'
endpoint2 = '/dayeonie/stt'


def fx_seek(self, frames, whence=os.SEEK_SET):
    self._check_if_closed()
    position = sf._snd.sf_seek(self._file, frames, whence)
    return position


def fx_get_format_from_filename(file, mode):
    format = ''
    file = getattr(file, 'name', file)
    try:
        format = os.path.splitext(file)[-1][1:]
        format = format.decode('utf-8', 'replace')
    except Exception:
        pass
    if format == 'opus':
        return 'OGG'
    if format.upper() not in sf._formats and 'r' not in mode:
        raise TypeError("No format specified and unable to get format from "
                        "file extension: {0!r}".format(file))
    return format


sf._snd =  sf._ffi.dlopen('../libsndfile-1.0.29pre2/build/libsndfile.so')
sf._subtypes['OPUS'] = 0x0064
sf.SoundFile.seek = fx_seek
sf._get_format_from_filename = fx_get_format_from_filename

with open('/home/jeehoon/Music/.dayeonie/dayeonie.ogg', 'rb') as f:
    audioblob = f.read()

data, sr = sf.read('/home/jeehoon/Music/.dayeonie/dayeonie.ogg')
print(data)
data, sr = sf.read(io.BytesIO(audioblob), dtype='int16')

print(data)