import numpy as np
import random


def fb_features(wav_data, frame_size, frame_stride, n_fft, n_filter, concat=False, window_size=7, random_crop=False, crop_len=0, follow=False):
    # Read wav file
    sample_rate, data = wav_data
    if follow:
        print(f"\nAudio of length {len(data)} sample points with {sample_rate} sample rate!")

    # Randomly crop to a given length
    if random_crop:
        crop_range = int(sample_rate * crop_len)
        start_pt = random.randint(0, len(data) - crop_range)
        data = data[start_pt:start_pt + crop_range]
        if follow:
            print(f"\nRandomly cropped {crop_len}s worth of data from the entire sound track")
            print(f"Resulting in a data length of {data.shape}")

    # Pre-emphasis - Balance High freq. and Low freq. magnitude
    pre_emphasis = 0.97
    emphasized_signal = np.append(data[0], data[1:] - pre_emphasis * data[:-1])

    # ---------------------------------

    # Divide signal into frames - For short time fourier transform
    frame_length, frame_step = frame_size * sample_rate, frame_stride * sample_rate
    frame_length = int(round(frame_length))
    frame_step = int(round(frame_step))
    if follow:
        print(f"\nDividing the sample array into frames of length {frame_length}, every {frame_step} data points")

    # Signal into frames of length 'frame_length' sampled at every stride of 'frame_step'
    signal_length = len(emphasized_signal)
    num_frames = int(np.ceil(float(np.abs(signal_length - frame_length)) / frame_step))

    # Pad signal such that the frames fit in completely
    pad_signal_length = num_frames * frame_step + frame_length
    z = np.zeros((pad_signal_length - signal_length))
    pad_signal = np.append(emphasized_signal, z)

    # fill in the signal values per frame
    indices = np.tile(np.arange(0, frame_length), (num_frames, 1)) + np.tile(
        np.arange(0, num_frames * frame_step, frame_step), (frame_length, 1)).T
    frames = pad_signal[indices.astype(np.int32, copy=False)]
    if follow:
        print(f"Resulting in {frames.shape[0]} frames, each of length {frame_length}\nData shape : {frames.shape}")
    # -----------------------------------

    # Apply a Hamming window function to each frame - focus on central part of frame
    frames *= np.hamming(frame_length)

    # ------------------------------------

    # Short time Discrete Fourier-Transform and Power Spectrum
    mag_frames = np.absolute(np.fft.rfft(frames, n_fft))
    # This will result in (n_fft/2 + 1) or ((n_fft+1)/2) possible frequencies -> then get magnitude of each freq.
    # Resulting in shape [num_frames, (n_fft/2 + 1)]

    pow_frames = ((1.0 / n_fft) * ((mag_frames) ** 2))
    if follow:
        print(f"\nLooking for {n_fft//2 + 1} distinct frequencies from the given sequence")
        print(f"Data now looks like {pow_frames.shape[0]} frames holding {pow_frames.shape[1]} distinct frequency magnitude data")
    # ----------------------------------

    # Filter Banks
    # n_filter amounts of equally spaced melodic scale frequency
    low_freq_mel = 0
    high_freq_mel = (2595 * np.log10(1 + (sample_rate / 2) / 700))
    mel_points = np.linspace(low_freq_mel, high_freq_mel, n_filter + 2)

    # Converted in the hz scale (log scaled)
    hz_points = (700 * (10 ** (mel_points / 2595) - 1))

    # Scale the hz_points frequency distribution (governed by sample rate) to the Fourier Transformed length (n_fft)
    bin = np.floor((n_fft + 1) * hz_points / sample_rate)
    if follow:
        print(f"\n{n_fft//2 + 1} distinct points are conceptual, we need to scale the distinctions into our actual data spectrum(sample rate)")

    # Filter banks initialization
    fbank = np.zeros((n_filter, int(np.floor(n_fft / 2 + 1))))
    if follow:
        print(f"\nThe fourier tranformed frequencies are going to be organised into {n_filter} bins of magnitude to organise freq. data into our desired dimension")

    # Define the triangular filter over the frequencies
    for m in range(1, n_filter + 1):
        f_m_minus = int(bin[m - 1])
        f_m = int(bin[m])
        f_m_plus = int(bin[m + 1])

        for k in range(f_m_minus, f_m):
            fbank[m - 1, k] = (k - bin[m - 1]) / (bin[m] - bin[m - 1])

        for k in range(f_m, f_m_plus):
            fbank[m - 1, k] = (bin[m + 1] - k) / (bin[m + 1] - bin[m])

    # Apply the triangular filters to the fourier transformed magnitudes
    filter_banks = np.dot(pow_frames, fbank.T)

    # For numerical stability
    filter_banks = np.where(filter_banks == 0, np.finfo(float).eps, filter_banks)

    # Back to linear scale -> Decibels
    filter_banks = 20 * np.log10(filter_banks)

    # ----------------------------------

    # Mean-normalize the filter banks (extract meaningful distinctions)
    filter_banks -= (np.mean(filter_banks, axis=0) + 1e-8)

    if not concat:
        return filter_banks
    else:
        if follow:
            print(f"In the end, we end up with each of the frames being represented with {n_filter} bins, holding ranges of the frequencies")
            print(f"We end up with data of shape: {filter_banks.shape}")

        pad = np.zeros((window_size, n_filter))
        padded_filter_banks = np.concatenate([pad, filter_banks, pad])

        # Feature concatenation
        concat_filters = []
        for i in range(window_size, len(filter_banks) + window_size):
            cf = padded_filter_banks[(i - window_size):(i + window_size + 1)]
            concat_filters.append(cf)
        concat_filters = np.stack(concat_filters).reshape(-1, (window_size * 2 + 1) * n_filter)
        if follow:
            print(f"\nConcatenating {window_size} frames before and after the current frame all together results in {2*window_size+1} frames worth of data")
            print(f"This results in the final data shape of {concat_filters.shape} = ({num_frames}, {n_filter} * {2*window_size+1})")
        return concat_filters    


def matcher(found_pairs, final_dict):
    # Get values of the given dictionary
    fpv = list(found_pairs.values())
    
    # Sort the dictionary by each of the value's smallest score 
    sorted_pairs = sorted(list(zip(found_pairs, fpv)), key=lambda x: sorted(x[1], key=lambda y: y[1])[0][1])

    # Get the best matching
    speaker = sorted_pairs[0][0]
    selected = min(sorted_pairs[0][1], key=lambda x: x[1])
    final_dict.update({speaker: selected[0]}) # Finalize matching

    # Remove found match from other keys and remove matched key itself
    for k, v in found_pairs.items():
        for i, x in enumerate(v):
            if x[0] == selected[0]:
                del found_pairs[k][i]
    found_pairs.pop(speaker)

    # Recursively iterate until there are no more unmatched speakers
    if len(found_pairs) > 0:
        matcher(found_pairs, final_dict)