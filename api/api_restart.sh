#!/bin/bash

ps auxw | grep gunicorn | grep -v grep > /dev/null

if [ $? != 0 ]
then
        nohup sudo ../../drtc/bin/gunicorn --certfile=/etc/letsencrypt/live/deeprtc.ml/fullchain.pem --keyfile=/etc/letsencrypt/live/deeprtc.ml/privkey.pem -b 0.0.0.0:1999 nnmain:app > nnapi.log &

else
        sudo kill -9 $(ps ax | grep gunicorn | grep -v grep | awk '{print $1}')

        nohup sudo ../../drtc/bin/gunicorn --certfile=/etc/letsencrypt/live/deeprtc.ml/fullchain.pem --keyfile=/etc/letsencrypt/live/deeprtc.ml/privkey.pem -b 0.0.0.0:1999 nnmain:app > nnapi.log &


fi
