// Get form element from index page
const form = document.getElementById('chooseRoom');

// Form submit event handler
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get our form from the index page
    form.addEventListener('submit', (event) => {

      if (form.checkValidity() === false){
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    });
  }, false);
})();

// Ajax query to show modal if room exists/ doesn't exist
$(function () {
  $.ajax({
    url: "/api/ROOM_IO"
    }).done(function(res) {
      switch(res['status']) {
        case 1:
          $('#roomExists').modal();
          break;
        case 2:
          $('#roomNonexists').modal()
          break;
        case 3:
          $('#roomFull').modal()
          break;
      }
    });
  }
)
