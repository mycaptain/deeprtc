import ssl
from aiohttp import web
import socketio
import requests

# Socket.IO - transport protocol, real-time bidirectional event based communication

# SSL
SSL = False

# Dictionary to manage clients per room
clients = {}

# Socket.IO Asynchronous server
sio = socketio.AsyncServer(cors_allowed_origins="*")

# Asynchronous HTTP server with aiohttp (asyncio + Python)
app = web.Application()

# Attach the Socket.IO server to the aiohttp application
sio.attach(app)


# Events transmitted over websocket
# When client wants to communicate with server, it emits an event
# sid - session ID per client


# Get room code of room that contains the queried session ID
def get_room_code(sid):
    room = None
    for r in clients.keys():
        if sid in clients[r]:
            room = r
            break

    return room


# Connect Event
@sio.event
async def connect(sid, environ):
    global clients

    # Get room query from socket connection
    room =  environ['QUERY_STRING'].split("&")[0].split('=')[1]
    print(f"Room: {room} <- Connected {sid}")

    # Add to clients dict
    clients[room] = clients.get(room, [])
    clients[room].append(sid)
    print(clients)

    # Server send 'ready' event to every client in room except for event originator
    await sio.emit('ready', clients[room].index(sid), room=room, skip_sid=sid)

    # Send post to frontend to tell that client has joined
    requests.post("http://localhost:5000/room_control/join", json={"room": room})

    # Enter room after 'ready'
    sio.enter_room(sid, room)


# Disconnect Event
@sio.event
async def disconnect(sid):
    global clients

    # Get room code of client
    room = get_room_code(sid)
    
    # Disconnect function
    # await sio.emit('disconnect', clients[room].index(sid), room=room, skip_sid=[x for x in clients[room] if x != sid])
    await sio.emit('disconnect', clients[room].index(sid), room=room, skip_sid=sid)

    # Leave room
    sio.leave_room(sid, room)
 
    # Remove from clients' list and delete room key if no participants left
    clients[room].remove(sid)  
    if len(clients[room]) == 0:
        del clients[room]

    # Send post to frontend to tell that client left room
    requests.post("http://localhost:5000/room_control/left", json={"room": room})
    
    print(f"Room: {room} -> Disconnected {sid}")
    print(clients)


# Transfer Data event
@sio.event
async def data(sid, data):
    global clients
    room = get_room_code(sid)
    # print("Message from {}: {}".format(sid, data))

    # Get client id number 
    c_idx = clients[room].index(sid)

    # Get Peer Connection from the event data
    pc_to = data['pc_idx']

    # For browsers that were there before this browser, it uses the peer connection matching it's appearance order
    if pc_to < c_idx:
        data['pc_idx'] = c_idx - 1

    # For browsers that connected after this browser, those browsers reference this browser as it's appearance order
    else:
        data['pc_idx'] = c_idx
        pc_to += 1  # +1 as myself is not included

    # Relay the data (Offer, Answer, ICE Candidate, etc) to the relevant client, with the modified pc_idx
    await sio.emit('data', data, room=room, skip_sid=[x for i ,x in enumerate(clients[room]) if i != pc_to])


# Start the aiohttp server for websocket communication
if __name__ == "__main__":
    if SSL:
        ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        certFile = '/etc/letsencrypt/live/deeprtc.ml/fullchain.pem'
        keyFile = '/etc/letsencrypt/live/deeprtc.ml/privkey.pem'
        ssl_context.load_cert_chain(certFile, keyFile)
        web.run_app(app, ssl_context=ssl_context, port=1403)
    else:
        web.run_app(app, port=1403)
