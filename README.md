# WebRTC + Deep Learning

- Developed basic web front-end using Javascript and HTML
- Using socket.io to communicate with backend signalling server (Python aiohttp, socket.io server)
- Flask API server running separately
- All of the servers up in AWS server
- Flask app is up with Gunicorn and SSL certificate
- Domain name bought and namespace connected to AWS instance
- nginx as web proxy and server for the front-end
- coturn as TURN server for the browser application
- SSL certificate issued by letsencrypt
- 


## References
[1] https://pfertyk.me/2020/03/webrtc-a-working-example/ <br>
[2] https://medium.com/av-transcode/what-is-webrtc-and-how-to-setup-stun-turn-server-for-webrtc-communication-63314728b9d0
[3] https://www.nginx.com/blog/setting-up-nginx/
[4] https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04